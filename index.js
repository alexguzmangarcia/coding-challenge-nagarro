/**
 * Make the middle section of the string contain only unrepeated charachters to count them.
 * @param {string} str
 * @returns string containing the unrepeated chars
 */
function makeUniqueString(str) {
  // Uses the spread syntaxis to convert the set to an array to fet the length of it
  return [...new Set(str)];
}
/**
 * Divide the string seppparated by spaces, so the program knows which strings it has to reformat, then iterating them to apply the logic of the reformat
 * @param {string} stringToReplace
 * @returns new string with the required format (first letter, number of distinct characters between first and last character, and last letter).
 */
function stringReplacer(stringToReplace) {
  if (typeof stringToReplace !== "string")
    throw "Unable to reformat, please check the information sent to the function.";
  const arrayOfStrings = stringToReplace.split(" "),
    rearrangedStringArray = [];
  for (const sentence of arrayOfStrings) {
    const firstChar = sentence.charAt(0),
      lastChar = sentence.slice(-1),
      middleSection = sentence.substr(1, sentence.length - 2),
      middleCount = makeUniqueString(middleSection).length;
    rearrangedStringArray.push(`${firstChar}${middleCount}${lastChar}`);
  }

  return rearrangedStringArray.join(" ");
}

const originalString = "Smooth";
try {
  const replacedString = stringReplacer(originalString);
  console.log(replacedString);
} catch (error) {
  console.log(error);
}

module.exports = stringReplacer;
