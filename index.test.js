const stringReplacer = require("./index");

test("Reformat the string 'Smooth' to match with the characteristics of the challenge ('S3h')", () => {
  expect(stringReplacer("Smooth")).toMatch("S3h");
});
test("Send a non-string parameter to the function expecting to catch the error", () => {
  expect(() => {
    stringReplacer(null);
  }).toThrow(
    "Unable to reformat, please check the information sent to the function."
  );
});
